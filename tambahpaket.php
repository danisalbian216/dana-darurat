<?php 
include 'koneksi.php';
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link href="img/logo.png" rel="icon">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/bootstrap.min.css">

    <title>Form Tambah Menu</title>
  </head>
  <body>
  <div class="container">
    <h3 class="text-center mt-3 mb-5">Tambah Produk</h3>
    <div class="card p-5 mb-5">
      <form method="POST" action="" enctype="multipart/form-data">
        <div class="form-group">
          <label for="nama_paket">Nama Paket</label>
          <input type="text" name="nama_paket" id="nama_paket" placeholder="Paket A" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="bunga_paket">Bunga Paket</label>
          <input type="text" name="bunga_paket" id="bunga_paket" placeholder="1%" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="cicilan_paket">Cicilan Paket</label>
          <input typetype="text" name="cicilan_paket" id="cicilan_paket" placeholder="Cicilan" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="jumlah_pinjaman">Jumlah Pinjaman</label>
          <input type="text" class="form-control" name="jumlah_pinjaman" id="jumlah_pinjaman" placeholder="10000" required>
        </div>
        <button type="submit" class="btn btn-primary" name="tambah">Tambah</button>
        <button type="reset" class="btn btn-danger" name="reset">Hapus</button>
      </form>

      <?php
       if (isset($_POST['tambah'])) {
              $nama_paket = $_POST['nama_paket'];
              $bunga_paket = $_POST['bunga_paket'];
              $cicilan_paket = $_POST['cicilan_paket'];
              $jumlah_pinjaman = $_POST['jumlah_pinjaman'];



              $insert = mysqli_query($conn, "INSERT INTO paket_pinjaman VALUES (NULL, '$nama_paket', '$bunga_paket', '$cicilan_paket', '$jumlah_pinjaman')");

              if ($insert) {
                     header("location: produkadmin.php");
              } else {
                     echo "Maaf, terjadi kesalahan";
              }
       }
       ?>
  </div>
  </div>
  
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
  </body>
</html>