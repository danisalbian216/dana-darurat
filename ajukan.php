<?php
include 'koneksi.php';
$id_paket = $_GET['id_paket'];

// if (isset($_GET['id_paket'])) {
//        echo $_GET['id_paket'];
// } else {
//        echo "id Paket belum dipilih";
// };
// die();


?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link href="img/logo.png" rel="icon"> 

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/bootstrap.min.css">

    <title>Pengajuan Pinjaman</title>
  </head>
  <body>
  <div class="container">
    <h3 class="text-center mt-3 mb-5">Pengajuan Pinjaman</h3>
    <div class="card p-5 mb-5">
      <form method="POST" action="" enctype="multipart/form-data">
        <div class="form-group">
          <label for="nama_peminjam">Nama</label>
          <input type="text" name="nama_peminjam" id="nama_peminjam" placeholder="Masukkan Nama Anda" class="form-control" required>
        </div>
        <div class="form-group">
        <label for="gender_peminjam">Gender</label>
        <select name="gender_peminjam" id="gender_peminjam" placeholder="gender" required>
                     <option selected>Tentukan Gender</option>
                     <option value="laki-laki">Laki-Laki</option>
                     <option value="perempuan">Perempuan</option>
            </select>
        </div>
        <div class="form-group">
          <label for="umur_peminjam">Umur</label>
          <input type="number" name="umur_peminjam" id="umur_peminjam"  class="form-control" placeholder="Masukkan Umur Anda" required>
        </div>
        <div class="form-group">
          <label for="alamat_peminjam">Alamat</label>
          <input type="text" class="form-control" type="text" name="alamat_peminjam" id="alamat_peminjam" placeholder="Masukkan Alamat" required>
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="text" name="email" id="email" id="alamat_peminjam" placeholder="Masukkan Email anda" class="form-control" required>
        </div>
        <button type="submit" class="btn btn-primary" name="tambah">Ajukan</button>
      </form>

      <?php
       if ($_POST) {
              $sql = "INSERT INTO datapeminjam (id_paket, nama_peminjam, gender_peminjam, umur_peminjam, alamat_peminjam, email) VALUES ('$id_paket', '{$_POST['nama_peminjam']}', '{$_POST['gender_peminjam']}', '{$_POST['umur_peminjam']}', '{$_POST['alamat_peminjam']}', '{$_POST['email']}')";
              $query = mysqli_query($conn, $sql);

              $sql = "SELECT id_paket FROM paket_pinjaman ";
              $query = mysqli_query($conn, $sql);

              $data = mysqli_fetch_array($query);
              $id_paket = $data['id_paket'];

              if ($query) {
                     echo "Data berhasil disimpan";
                     header('Location: pengajuan.php');
              } else {
                     echo "Data gagal disimpan";
              }
       }
       ?>
  </div>
  </div>
  
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
  </body>
</html>