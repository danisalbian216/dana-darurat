<?php
session_start();
include 'koneksi.php';
?>

<?php

if ($_SESSION['status'] == "") {
    header("location:index.php?pesan=gagal");
}

// if (!isset($_SESSION['status_user'])) {
//     // header("Location: navbar.php");
//     echo 'user belum ada di session';
// }

// if (!isset($_SESSION['role_user'])) {
//     // header("Location: eror.php");
// }
?>

<!DOCTYPE html>
<html lang="en">

<?php if ($_SESSION['status'] == "user") { ?>

<head>
    <meta charset="utf-8">
    <title>Dana Darurat</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <link href="img/logo.png" rel="icon">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"> 

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <div class="container-xxl bg-white p-0">
        <div class="container-xxl position-relative p-0">
            <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
                <a href="" class="navbar-brand p-0">
                    <h1 class="m-0"><img src="img/logo.png" alt="Logo"><span class="fs-5">Dana Darurat</span></h1>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav ms-auto py-0">
                        <a href="user.php" class="nav-item nav-link active">Home</a>
                        <a href="#" class="nav-item nav-link">Bantuan</a>
                        <a href="#" class="nav-item nav-link">Contact</a>
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Dana Darurat</a>
                            <div class="dropdown-menu m-0">
                                <a href="pengajuan.php" class="dropdown-item">Pengajuan</a>
                                <a href="profil.php" class="dropdown-item">Profil</a>
                            </div>
                        </div>
                    </div>
                    <a href="logout.php" class="btn btn-secondary text-light rounded-pill py-2 px-4 ms-3">Logout</a>
                </div>
            </nav>

            <div class="container-xxl py-5 bg-primary hero-header mb-5">
                <div class="container my-5 py-5 px-lg-5">
                    <div class="row g-5 py-5">
                        <div class="col-12 text-center">
                        <h1 class="text-white animated zoomIn">Selamat Datang</h6>
                        <h6 class="text-white animated zoomIn">Hallo  <?php echo $_SESSION['username']; ?> Sekarang Anda Login  Sebagai <?php echo $_SESSION['status']; ?></h2>
                        <hr class="bg-white mx-auto mt-0" style="width: 90px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php } ?>
        
        <footer class="footer-16371">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-9 text-center">
            <div class="copyright">
              <p class="mb-0"><small>&copy; Albelial. All Rights Reserved.</small></p>
            </div>
          </div>
        </div>
      </div>
    </footer>

        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top pt-2"><i class="bi bi-arrow-up"></i></a>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="lib/isotope/isotope.pkgd.min.js"></script>
    <script src="lib/lightbox/js/lightbox.min.js"></script>

    <script src="js/main.js"></script>
</body>
</html>