<?php
include 'koneksi.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="img/logo.png" rel="icon">
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">
    <link rel="stylesheet" href="./css/login.css">

    <title>Login</title>
</head>

<body>

    <div class="container">
        <div class="forms">
            <div class="form login">
                <span class="title">Login</span>

                <form class="" action="fungsi_login.php" method="POST">
                    <div class="input-field">
                        <label for="username" class="text">Username</label><br>
                        <input type="text" placeholder="Enter your username" name="username" id="username" required value="">
                    </div>                   
                    <div class="input-field">
                        <label for="password" class="text">Password</label><br>
                        <input type="password" class="password" placeholder="Enter your password" name="password" id="password" required value="">
                    </div><br>
                        <div class="input-field submit">
                        <input type="submit" name="login" type="button" value="Login">
                        </div>
                </form>

                <div class=" login-signup">
                    <span class="text">Not a member?
                        <a href="register.php" class="text signup-link">Signup Now</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
    
</body>

</html>