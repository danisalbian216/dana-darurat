<?php
include 'koneksi.php';
$id_paket = $_GET['id_paket'];

$ambil = mysqli_query($conn, "SELECT * FROM paket_pinjaman WHERE id_paket='$id_paket'");
$result = mysqli_fetch_assoc($ambil);
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link href="img/logo.png" rel="icon">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/bootstrap.min.css">

    <title>Dana Darurat</title>
  </head>
  <body>
  <div class="container">
    <h3 class="text-center mt-3 mb-5">Edit Produk</h3>
    <div class="card p-5 mb-5">
      <form  method="POST" action="edit.php" enctype="multipart/form-data">
        <div class="post">
        <input type="hidden" name="id_paket" value='<?= $result['id_paket']; ?>'>
            <div class="form-group">
                <label for="nama_paket">Nama Paket</label>
                <input type="text" type="text" name="nama_paket" value='<?= $result['nama_paket']; ?>' placeholder="Paket A"  class="form-control" required>
            </div>
            <div class="form-group">
                <label for="bunga_paket">Bunga Paket</label>
            <input type="text" name="bunga_paket" value='<?= $result['bunga_paket']; ?>' placeholder="1%t" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="cicilan_paket">Cicilan Paket</label>
                <input type="text" name="cicilan_paket" value='<?= $result['cicilan_paket']; ?>' placeholder="10x" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="jumlah_pinjaman">Jumlah Pinjaman</label>
                <input type="text" name="jumlah_pinjaman" value='<?= $result['jumlah_pinjaman']; ?>' placeholder="10000" class="form-control" required>
            </div>
        </div>
        <button type="submit" class="btn btn-primary" name="tambah">Edit</button>
        <button type="reset" class="btn btn-danger" name="reset">Hapus</button>
      </form>
  </div>
  </div>
  
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
  </body>
</html>