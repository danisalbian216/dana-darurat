
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Pengajuan</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <link href="img/logo.png" rel="icon">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"> 

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="css/riwayat.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <div class="container-xxl bg-white p-0">
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-grow text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <div class="container-xxl position-relative p-0">
            <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
                <a href="" class="navbar-brand p-0">
                    <h1 class="m-0"><img src="img/logo.png" alt="Logo"><span class="fs-5">Dana Darurat</span></h1>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav ms-auto py-0">
                        <a href="user.php" class="nav-item nav-link active">Home</a>
                        <a href="#" class="nav-item nav-link">Bantuan</a>
                        <a href="#" class="nav-item nav-link">Contact</a>
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Dana Darurat</a>
                            <div class="dropdown-menu m-0">
                                <a href="pengajuan.php" class="dropdown-item">Pengajuan</a>
                                <a href="profil.php" class="dropdown-item">Profil</a>
                            </div>
                        </div>
                    </div>
                    <a href="logout.php" class="btn btn-secondary text-light rounded-pill py-2 px-4 ms-3">Logout</a>
                </div>
            </nav>

            <div class="container-xxl py-5 bg-primary hero-header mb-5">
                <div class="container my-5 py-5 px-lg-5">
                    <div class="row g-5 py-5">
                        <div class="col-12 text-center">
                            <h1 class="text-white animated zoomIn">Halaman Pengajuan</h1>
                            <hr class="bg-white mx-auto mt-0" style="width: 90px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="searchModal" tabindex="-1">
            <div class="modal-dialog modal-fullscreen">
                <div class="modal-content" style="background: rgba(29, 29, 39, 0.7);">
                    <div class="modal-header border-0">
                        <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body d-flex align-items-center justify-content-center">
                        <div class="input-group" style="max-width: 600px;">
                            <input type="text" class="form-control bg-transparent border-light p-3" placeholder="Type search keyword">
                            <button class="btn btn-light px-4"><i class="bi bi-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container-xxl py-5">
            <div class="container px-lg-5">
                <div class="row g-5">
                    <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="section-title position-relative mb-4 pb-2">
                            <h6 class="position-relative text-primary ps-4">User</h6>
                            <h2 class="mt-2">Dana Darurat Kini Menawrkan Produk Pengajuan   </h2>
                        </div>
                        <div class="container"> 
                    </div>
                </div>
                <div class="container">
       
      <div class="row mt-3">
      <?php

include('koneksi.php');

$query = mysqli_query($conn, 'SELECT * FROM paket_pinjaman');
$result = mysqli_fetch_all($query, MYSQLI_ASSOC);

?>

<?php foreach ($result as $result) : ?>

<div class="col-md-3 mt-4">
  <div class="card brder-dark"> 
  <img src="./img/logo.png" class="imgg" alt="...">
    <div class="card-body">
    <h5 class="card-title text-center"><?php echo $result['nama_paket'] ?></h5>
    <h6 class="card-title"><strong>Rp. </strong><?php echo number_format($result['jumlah_pinjaman']); ?></h6>
    <h6 class="card-text"><strong>Cicilan : </strong><?php echo $result['bunga_paket'] ?>x</h6>
    <h6 class="card-text"><strong>Bunga   : </strong><?php echo $result['cicilan_paket'] ?>%</h6>
    <a href="ajukan.php?id_paket=<?php echo $result['id_paket'] ?>" class="btn btn-success btn-sm btn-block ">Ajukan Pinjaman</a>
    <input type="hidden" name="id_paket" id="paket" value="id_paket">
    </div>
  </div>
</div>
<?php endforeach; ?>
    </div>
            </div>
        </div>
        <footer class="footer-16371">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-9 text-center">
            <div class="copyright">
              <p class="mb-0"><small>&copy; Albelial. All Rights Reserved.</small></p>
            </div>
          </div>
        </div>
      </div>
    </footer>
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top pt-2"><i class="bi bi-arrow-up"></i></a>
    </div>
</div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="lib/isotope/isotope.pkgd.min.js"></script>
    <script src="lib/lightbox/js/lightbox.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>