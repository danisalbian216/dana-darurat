<?php
include 'koneksi.php';
$id_data = $_GET['id'];

$sql = "SELECT * FROM datapeminjam d INNER JOIN paket_pinjaman p ON d.id_paket = p.id_paket where d.id_data='$id_data'";
$query = mysqli_query($conn, $sql);
$data = mysqli_fetch_array($query);
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link href="img/logo.png" rel="icon">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/bootstrap.min.css">

    <title>Dana Darurat</title>
  </head>
  <body>
  <div class="container">
    <h3 class="text-center mt-3 mb-5">Edit Peminjam</h3>
    <div class="card p-5 mb-5">
      <form  action="actionedit.php" method="post" enctype="multipart/form-data">
        <div class="post">
        <input type="hidden" name="id_data" value="<?= $data['id_data'] ?>">
            <div class="form-group">
                <label for="nama_peminjam">Nama</label>
                <input type="text" name="nama_peminjam" value="<?= $data['nama_peminjam'] ?>" class="form-control" required>
            </div>
            <div class="form-group">
            <select name="gender_peminjam" id="gender_peminjam" placeholder="gender" class="form-control" required>
              <option selected>Tentukan Gender</option>
              <option value="laki-laki">Laki-Laki</option>
              <option value="perempuan">Perempuan</option>
             </select>
            </div>
            <div class="form-group">
                <label for="umur_peminjam">Umur</label>
                <input type="number" name="umur_peminjam" value="<?= $data['umur_peminjam'] ?>" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="alamat_peminjam">Alamat</label>
                <input type="text" name="alamat_peminjam" value="<?= $data['alamat_peminjam'] ?>" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
            <input type="text" name="email" value="<?= $data['email'] ?>" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <select name="keterangan" id="keterangan" placeholder="Keterangan" class="form-control" required>
                  <option value="Disetujui">Disetujui</option>
                  <option value="Ditolak">Ditolak</option>
                </select>
            </div>
            <div class="form-group">
              <label for="nama_paket"></label>
            <select name="nama_paket" class="form-control">
       <?php 
       $query1 = mysqli_query($conn,"SELECT * FROM paket_pinjaman");
       while($data1 = mysqli_fetch_array($query1)) {
              if($data['d.id_paket'] == $data1['id_paket']){
                     $terselect ="selected";
              }else{
                     $terselect ="";
              }
              echo "<option value='".$data1['id_paket']."'$terselect>"
              .$data1['nama_paket']."</option>";
       }
       ?>
       </select>
            </div>
        </div>
        <button type="submit" class="btn btn-primary" name="tambah">Edit</button>
        <button type="reset" class="btn btn-danger" name="reset">Hapus</button>
      </form>
  </div>
  </div>
  
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
  </body>
</html>